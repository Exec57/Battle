﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Battle
{
    public enum Data
    {
        WorldInfo = 1,
        World = 2,
        PlayerList = 3,
        PlayerPos = 4,
        PlayerDisconnect = 5,
        Block = 6,
        Time = 7
    }

    public class Client
    {
        NetClient netClient;
        NetConnectionStatus connectionStatus;
        NetIncomingMessage inc;
        Map map;

        public bool Connected
        {
            get { return connectionStatus == NetConnectionStatus.Connected; }
        }

        public Client()
        {
            netClient = new NetClient(new NetPeerConfiguration("BattleGame"));
            netClient.Start();
            netClient.Configuration.ConnectionTimeout = 5;
        }

        public void Update(GameTime gameTime)
        {
            while ((inc = netClient.ReadMessage()) != null)
            {
                switch (inc.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        Data data = (Data)inc.ReadByte();
                        if (data == Data.WorldInfo)
                            map.ReadInfo(inc);
                        else if (data == Data.World)
                            map.Read(inc);
                        else if (data == Data.PlayerList)
                            map.ReadPlayerList(inc);
                        else if (data == Data.PlayerPos)
                            map.ReadPlayerPos(inc);
                        else if (data == Data.PlayerDisconnect)
                            map.RemovePlayer(map.GetPlayer(inc.ReadByte()));
                        else if (data == Data.Block)
                            map.ChangeBlock(inc);
                        else if (data == Data.Time)
                            map.lightAlpha = inc.ReadFloat();
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)inc.ReadByte();
                        connectionStatus = status;

                        if (connectionStatus == NetConnectionStatus.Connected)
                        {
                            Main.map = new Map();
                            map = Main.map;
                        }
                        else if (connectionStatus == NetConnectionStatus.Disconnected)
                        {
                            Main.map = null;
                            Main.menu = new MenuMain();
                        }
                        break;
                }
            }
        }

        public void Connect(string ip, int port)
        {
            netClient.Connect(ip, port);
        }

        public NetOutgoingMessage CreateMessage()
        {
            return netClient.CreateMessage();
        }

        public void SendMessage(NetOutgoingMessage outmsg, NetDeliveryMethod method, int sequenceChannel = 0)
        {
            netClient.SendMessage(outmsg, method, sequenceChannel);
        }

        public void Stop()
        {
            netClient.Disconnect("");
        }
    }
}
