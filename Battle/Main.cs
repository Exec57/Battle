﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace Battle
{
    public class Main : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        public static int screenWidth = 800;
        public static int screenHeight = 600;
        public static Texture2D rectTexture;
        public static float scale = 3;
        public static SpriteFont font;
        public static MouseState mouse;
        public static MouseState lastMouse;
        public static KeyboardState keyboard;
        public static KeyboardState lastKeyboard;
        public static Rectangle mouseRectangle;
        public static bool exit;
        public static Client client;
        public static bool active;
        public static int wheelValue;
        public static int oldWheelValue;

        static int port;
        static string ip = "localhost";

        public static Texture2D buttonTexture;
        public static Texture2D backgroundTexture;
        public static Texture2D playerTexture;
        public static Texture2D playerMaskTexture;
        public static Dictionary<string, Texture2D> blockTexture;

        public static Menu menu;
        public static Map map;
  
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;
            IsMouseVisible = true;
            graphics.IsFullScreen = false;
        }

        protected override void Initialize()
        {
            

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            rectTexture = new Texture2D(GraphicsDevice, 1, 1);
            rectTexture.SetData(new Color[] { Color.White });

            font = Content.Load<SpriteFont>("Font");

            buttonTexture = Content.Load<Texture2D>("Gui/Button");
            backgroundTexture = Content.Load<Texture2D>("Gui/Background");
            playerTexture = Content.Load<Texture2D>("Player");
            playerMaskTexture = Content.Load<Texture2D>("PlayerMask");

            blockTexture = new Dictionary<string, Texture2D>();
            string[] files = Directory.GetFiles("Content/Blocks", "*.xnb");
            for (int i = 0; i < files.Length; i++)
            {
                string textureName = files[i].Split('\\', '.')[1];
                blockTexture.Add(textureName, Content.Load<Texture2D>("Blocks/" + textureName));
            }

            ReadIni();

            menu = new MenuMain();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            keyboard = Keyboard.GetState();
            mouse = Mouse.GetState();
            wheelValue = mouse.ScrollWheelValue;

            active = IsActive;

            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || exit)
                Exit();

            mouseRectangle = new Rectangle(mouse.X / (int)scale, mouse.Y / (int)scale, 1, 1);

            if (client != null)
                client.Update(gameTime);
            if (menu != null)
                menu.Update(gameTime);
            if (map != null)
                map.Update(gameTime);

            base.Update(gameTime);

            lastKeyboard = keyboard;
            lastMouse = mouse;
            oldWheelValue = wheelValue;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            if (menu != null)
            {
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None,
                    RasterizerState.CullNone, null, Matrix.CreateScale(scale));

                menu.Draw(spriteBatch);

                spriteBatch.End();
            }
            if (map != null)
                map.Draw(spriteBatch);

            base.Draw(gameTime);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);

            if (client != null)
                client.Stop();
        }

        public static void Connect()
        {
            client = new Client();
            client.Connect(ip, port);
        }

        public static bool PressKey(Keys key)
        {
            if (keyboard.IsKeyDown(key) && lastKeyboard.IsKeyUp(key))
                return true;
            return false;
        }

         static void ReadIni()
        {
            string file = "client.ini";
            if (!File.Exists(file))
                return;

            using (StreamReader sr = new StreamReader(file))
            {
                try
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        string[] args = line.Split('=');
                        if (args[0] == "port")
                            port = Convert.ToInt32(args[1]);
                        if (args[0] == "ip")
                            ip = args[1];
                    }
                   
                    sr.Close();
                }
                catch {  }
            }
        }
    }
}
