﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Lidgren.Network.Xna;
using System.Threading;

namespace Battle
{
    public struct MapInfo
    {
        public int nb_block;
        public float mulPer;
        public int offset;
    }

    public enum BlockID
    {
        None = 0,
        Dirt = 1,
        Stone = 2,
        Torch = 3
    }

    public class Map
    {
        public static int SIZE;
        public static bool DEBUG;

        public Random random;

        MapInfo info;
        List<Player> playerList;
        Player player;
        Block[,] blocks;
        bool ready;

        public float lightAlpha = 0;
        
        List<Block> visibleBlock;
        List<BlockTorch> torchs = new List<BlockTorch>();

        Point mapHalfIndex;

        public float LightAlpha
        {
            get { return lightAlpha; }
        }

        public MapInfo Info
        {
            get { return info; }
        }

        public List<Block> VisibleBlock
        {
            get { return visibleBlock; }
        }

        public Block[,] Blocks
        {
            get { return blocks; }
        }

        public Map()
        {
            playerList = new List<Player>();

            mapHalfIndex = new Point(Main.screenWidth / (int)Main.scale / Block.SIZE / 2 + 2,
                Main.screenHeight / (int)Main.scale / Block.SIZE / 2 + 2);

            random = new Random();
        }

        public void ReadInfo(NetIncomingMessage inc)
        {
            SIZE = inc.ReadInt32();
            info.nb_block = SIZE * SIZE;
            info.mulPer = 100.0f / info.nb_block;
            blocks = new Block[SIZE, SIZE];
        }

        public void Read(NetIncomingMessage inc)
        {
            for (int i = 0; i < 100; i++)
            {
                BlockID id = (BlockID)inc.ReadByte();
                BlockID oldId = (BlockID)inc.ReadByte();
                int x = info.offset / SIZE;
                int y = info.offset % SIZE;
                Block newBlock = GetNewBlock(id, this, x, y);
                blocks[x, y] = newBlock;
                if (newBlock.ID == BlockID.Torch)
                    torchs.Add((BlockTorch)newBlock);
                blocks[x, y].oldId = oldId;
                info.offset++;
            }

            if (info.offset == info.nb_block)
            {
                ready = true;
                Main.menu = null;
                UpdateLight();
            }
        }

        public void Update(GameTime gameTime)
        {
            if (!ready) return;
            if (player == null) return;

            visibleBlock = new List<Block>();

            for (int y = player.MapIndex.Y - mapHalfIndex.Y; y < player.MapIndex.Y + mapHalfIndex.Y; y++)
            {
                for (int x = player.MapIndex.X - mapHalfIndex.X; x < player.MapIndex.X + mapHalfIndex.X; x++)
                {
                    if (x < 0 || x >= SIZE || y < 0 || y >= SIZE)
                        continue;
                    if (!blocks[x, y].IsUpdateBlock)
                        blocks[x, y].Update(gameTime);
                    visibleBlock.Add(blocks[x, y]);
                }
            }

            player.LocalUpdate(gameTime);

            visibleBlock.Clear();
            visibleBlock = null;

            foreach (Player p in playerList)
                p.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!ready) return;
            if (player == null) return;

            spriteBatch.Begin();
            spriteBatch.Draw(Main.rectTexture, new Rectangle(0, 0, Main.screenWidth, Main.screenHeight), Color.Lerp(Color.LightCyan, Color.Black, lightAlpha / 200)); 
            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp,
                DepthStencilState.None, RasterizerState.CullNone, null, player.Transform);

            for (int y = player.MapIndex.Y - mapHalfIndex.Y; y < player.MapIndex.Y + mapHalfIndex.Y; y++)
            {
                for (int x = player.MapIndex.X - mapHalfIndex.X; x < player.MapIndex.X + mapHalfIndex.X; x++)
                {
                    if (x < 0 || x >= SIZE || y < 0 || y >= SIZE)
                        continue;
                    blocks[x, y].Draw(spriteBatch);
                }
            }

            player.LocalDraw(spriteBatch);
            foreach (Player p in playerList)
                p.Draw(spriteBatch);

            spriteBatch.End();

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.None, RasterizerState.CullNone);

            player.LocalDraw2D(spriteBatch);

            spriteBatch.End();
        }

        public void ReadPlayerList(NetIncomingMessage inc)
        {
            byte count = inc.ReadByte();
            for (int i = 0; i < count; i++)
            {
                byte id = inc.ReadByte();
                Vector2 position = inc.ReadVector2();
                Player p = new Player(id, this);
                p.SetPosition(position);

                if (i < playerList.Count)
                    continue;

                playerList.Add(p);
                if (player == null && i == count - 1)
                {
                    player = playerList[i];
                    player.SetAsLocalPlayer();
                }
            }
        }

        public void ReadPlayerPos(NetIncomingMessage inc)
        {
            byte id = inc.ReadByte();
            Player p = GetPlayer(id);
            if (p != null)
                p.ReceivePosition(inc);
        }

        public Player GetPlayer(byte id)
        {
            foreach (Player p in playerList)
                if (p.ID == id)
                    return p;
            return null;
        }

        public void RemovePlayer(Player player)
        {
            playerList.Remove(player);
        }

        public void ChangeBlock(NetIncomingMessage inc)
        {
            BlockID id = (BlockID)inc.ReadByte();
            Point pos = inc.ReadPoint();
            Block currentBlock = blocks[pos.X, pos.Y];
            if (currentBlock.ID == BlockID.Torch)
                torchs.Remove((BlockTorch)currentBlock);
            Block newBlock = GetNewBlock(id, this, pos.X, pos.Y);
            if (newBlock.ID == BlockID.Torch)
                torchs.Add((BlockTorch)newBlock);
            newBlock.oldId = (BlockID)inc.ReadByte();
            blocks[pos.X, pos.Y] = newBlock;
            currentBlock.OnDestroyed();
        }

        Block GetNewBlock(BlockID id, Map map, int x, int y)
        {
            if (id == BlockID.None)
                return new BlockNone(map, x, y);
            else if (id == BlockID.Dirt)
                return new BlockDirt(map, x, y);
            else if (id == BlockID.Stone)
                return new BlockStone(map, x, y);
            else if (id == BlockID.Torch)
                return new BlockTorch(map, x, y);
            return null;
        }

        public void UpdateLight()
        {
            foreach(BlockTorch t in torchs)
                t.UpdateNoLight();
            foreach (BlockTorch t in torchs)
                t.CalcLight();
        }
    }
}
