﻿using Lidgren.Network;
using Lidgren.Network.Xna;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle
{
    public class Player
    {
        int width = 8;
        int height = 8;
        Vector2 position;
        Vector2 origin;
        Rectangle rectangle;
        float speed = 100;
        Matrix transform;
        Vector2 centerPosition;
        Map map;
        Point mapIndex;
        byte id;
        Vector2 oldPosition;
        Client client;
        bool isLocalPlayer;
        Block pointBlock;
        Point mouseIndex;
        Point oldMouseIndex;
        BlockID currentBlock = BlockID.Dirt;

        public byte ID
        {
            get { return id; }
        }

        public Matrix Transform
        {
            get { return transform; }
        }

        public Point MapIndex
        {
            get { return mapIndex; }
        }

        public Player(byte id, Map map)
        {
            this.id = id;
            this.map = map;

            origin = new Vector2(width / 2, height / 2);
            centerPosition = new Vector2(Main.screenWidth / 2, Main.screenHeight / 2);
        }

        public void Update(GameTime gameTime)
        {
            mapIndex = new Point(((int)position.X + Block.SIZE / 2) / Block.SIZE, ((int)position.Y + Block.SIZE / 2) / Block.SIZE);

            UpdateTransform();
        }

        public void LocalUpdate(GameTime gameTime)
        {
            mouseIndex = new Point((Main.mouse.X / (int)Main.scale + (int)position.X + Block.SIZE / 2 - (int)centerPosition.X / (int)Main.scale) / Block.SIZE,
                (Main.mouse.Y / (int)Main.scale + (int)position.Y + Block.SIZE / 2 - (int)centerPosition.Y / (int)Main.scale) / Block.SIZE);

            if (client == null)
                client = Main.client;

            if (Main.keyboard.IsKeyDown(Keys.D) || Main.keyboard.IsKeyDown(Keys.Right))
                Move(new Vector2((float)gameTime.ElapsedGameTime.TotalSeconds * speed, 0));
            if (Main.keyboard.IsKeyDown(Keys.A) || Main.keyboard.IsKeyDown(Keys.Left) || Main.keyboard.IsKeyDown(Keys.Q))
                Move(new Vector2(-(float)gameTime.ElapsedGameTime.TotalSeconds * speed, 0));
            if (Main.keyboard.IsKeyDown(Keys.W) || Main.keyboard.IsKeyDown(Keys.Up) || Main.keyboard.IsKeyDown(Keys.Z))
                Move(new Vector2(0, -(float)gameTime.ElapsedGameTime.TotalSeconds * speed));
            if (Main.keyboard.IsKeyDown(Keys.S) || Main.keyboard.IsKeyDown(Keys.Down))
                Move(new Vector2(0, (float)gameTime.ElapsedGameTime.TotalSeconds * speed));

            if (Main.PressKey(Keys.F1))
                Map.DEBUG = !Map.DEBUG;

            if (Main.wheelValue < Main.oldWheelValue)
                currentBlock++;
            if (Main.wheelValue > Main.oldWheelValue)
                currentBlock--;
            if ((int)currentBlock > 3)
                currentBlock = (BlockID)3;
            if ((int)currentBlock < 1)
                currentBlock = (BlockID)1;

            if (!IsOutsideWorld(mouseIndex) && Main.active)
            {
                if (Main.mouse.LeftButton == ButtonState.Pressed)
                {
                    if (mouseIndex != oldMouseIndex || (mouseIndex == oldMouseIndex && Main.lastMouse.LeftButton == ButtonState.Released))
                        RequestChangeBlock(BlockID.None, mouseIndex);
                }
                if (Main.mouse.RightButton == ButtonState.Pressed)
                    RequestChangeBlock(currentBlock, mouseIndex);
            }
                
            if (!IsOutsideWorld(mouseIndex))
                pointBlock = map.Blocks[mouseIndex.X, mouseIndex.Y];
            else
                pointBlock = null;
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Main.playerTexture, position, null, Color.White, 0f, origin, 1, SpriteEffects.None, 0);

            if (!IsOutsideWorld(mapIndex))
                spriteBatch.Draw(Main.playerMaskTexture, position, null, map.Blocks[mapIndex.X, mapIndex.Y].LightColor, 0f, origin, 1, SpriteEffects.None, 0);
        }

        public void LocalDraw(SpriteBatch spriteBatch)
        {
            if (pointBlock != null && Main.active)
                spriteBatch.Draw(Main.rectTexture, pointBlock.Rectangle, new Color(50, 50, 50, 50));
        }

        public void LocalDraw2D(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(Main.font, "X: " + mapIndex.X, Vector2.Zero, Color.White);
            spriteBatch.DrawString(Main.font, "Y: " + mapIndex.Y, new Vector2(0, Main.font.LineSpacing), Color.White);

            spriteBatch.Draw(Main.blockTexture[currentBlock.ToString()], new Vector2(Main.screenWidth - Block.SIZE * Main.scale - 10, 10), null, Color.White,
                0f, Vector2.Zero, Main.scale, SpriteEffects.None, 0);
        }

        void Move(Vector2 velocity)
        {
            oldPosition = position;
            position += velocity;
            UpdateTransform();

            foreach (Block b in map.VisibleBlock)
            {
                if (rectangle.Intersects(b.Rectangle) && b.Solid)
                {
                    velocity.Normalize();

                    if (velocity.X > 0)
                        position.X = b.Position.X - b.Origin.X - origin.X;
                    else if (velocity.X < 0)
                        position.X = b.Position.X + b.Origin.X + origin.X;

                    if (velocity.Y > 0)
                        position.Y = b.Position.Y - b.Origin.X - origin.Y;
                    else if (velocity.Y < 0)
                        position.Y = b.Position.Y + b.Origin.Y + origin.Y;

                    UpdateTransform();
                    break;
                }
            }
            if (position != oldPosition)
                SendPosition();
        }

        void UpdateTransform()
        {
            rectangle = new Rectangle((int)position.X - (int)origin.X, (int)position.Y - (int)origin.Y, width, height);
            if (isLocalPlayer)
            {
                transform = Matrix.CreateScale(Main.scale) *
                Matrix.CreateTranslation(new Vector3(-position * Main.scale + centerPosition, 0));
            }
        }

        void SendPosition()
        {
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)Data.PlayerPos);
            outmsg.Write(ID);
            outmsg.Write(position);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableSequenced, 1);
        }

        public void ReceivePosition(NetIncomingMessage inc)
        {
            position = inc.ReadVector2();
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
        }

        public void SetAsLocalPlayer()
        {
            isLocalPlayer = true;
        }

        bool IsOutsideWorld(Point point)
        {
            if (point.X < 0 || point.Y < 0 || point.X >= Map.SIZE || point.Y >= Map.SIZE)
                return true;
            return false;
        }

        public void RequestChangeBlock(BlockID id, Point pos)
        {
            NetOutgoingMessage outmsg = client.CreateMessage();
            outmsg.Write((byte)Data.Block);
            outmsg.Write((byte)id);
            outmsg.Write(pos);
            client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);

            oldMouseIndex = mouseIndex;
        }
    }
}
