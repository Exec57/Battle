﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleServer
{
    public class BlockTorch : Block
    {
        public BlockTorch(int x, int y) : base(x, y)
        {
            id = BlockID.Torch;
        }
    }
}
