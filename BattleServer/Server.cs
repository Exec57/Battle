﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using System.Threading;
using Microsoft.Xna.Framework;
using Lidgren.Network.Xna;

namespace BattleServer
{
    public enum Data
    {
        WorldInfo = 1,
        World = 2,
        PlayerList = 3,
        PlayerPos = 4,
        PlayerDisconnect = 5,
        Block = 6,
        Time = 7
    }

    public class Server
    {
        NetServer netServer;
        NetPeerConfiguration config;
        Thread thread;
        Map map;

        public Server()
        {
            config = new NetPeerConfiguration("BattleGame");
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            config.ConnectionTimeout = 5;
        }

        public bool Start(int port)
        {
            if (port == 0)
                return false;
            try
            {
                config.Port = port;
                netServer = new NetServer(config);
                netServer.Start();

                thread = new Thread(Update);
                thread.Start();

                return true;
            }
            catch
            {
                return false;
            }
        }

        void Update()
        {
            while(true)
            {
                if (map == null)
                    map = Program.map;

                NetIncomingMessage inc;

                while ((inc = netServer.ReadMessage()) != null)
                {
                    switch (inc.MessageType)
                    {
                        case NetIncomingMessageType.ConnectionApproval:
                            if (Map.READY)
                                inc.SenderConnection.Approve();
                            else
                                inc.SenderConnection.Deny();
                            break;

                        case NetIncomingMessageType.Data:
                            Data data = (Data)inc.ReadByte();
                            if (data == Data.PlayerPos)
                                PlayerPos(inc);
                            if (data == Data.Block)
                                map.ChangeBlock(inc);
                            break;

                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)inc.ReadByte();
                            if (status == NetConnectionStatus.RespondedConnect)
                            {
                                Console.WriteLine("New connection...");
                            }
                            if (status == NetConnectionStatus.Connected)
                            {
                                Console.WriteLine("New player connected");
                                map.SendWorldInfo(inc);
                                map.SendWorld(inc);
                                map.NewPlayer(inc);
                            }
                            if (status == NetConnectionStatus.Disconnected)
                                Disconnect(inc);
                            break;
                    }
                }
            }
        }

        public NetOutgoingMessage CreateMessage()
        {
            return netServer.CreateMessage();
        }

        public void SendMessage(NetOutgoingMessage outmsg, NetConnection connection, NetDeliveryMethod method)
        {
            netServer.SendMessage(outmsg, connection, method);
        }

        public void SendToAll(NetOutgoingMessage outmsg, NetDeliveryMethod method, int sequenceChanel)
        {
            netServer.SendToAll(outmsg, null, method, sequenceChanel);
        }

        void PlayerPos(NetIncomingMessage inc)
        {
            byte id = inc.ReadByte();
            Vector2 position = inc.ReadVector2();
            map.ReadPlayerPos(id, position);

            NetOutgoingMessage outmsg = netServer.CreateMessage();
            outmsg.Write((byte)Data.PlayerPos);
            outmsg.Write(id);
            outmsg.Write(position);
            netServer.SendToAll(outmsg, inc.SenderConnection, NetDeliveryMethod.ReliableSequenced, 1);
        }

        void Disconnect(NetIncomingMessage inc)
        {
            Player p = map.GetPlayer(inc.SenderConnection);
            Console.WriteLine("Player " + p.ID + " disconnected");
            NetOutgoingMessage outmsg = netServer.CreateMessage();
            outmsg.Write((byte)Data.PlayerDisconnect);
            outmsg.Write(p.ID);
            netServer.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered);
            map.RemovePlayer(p);
        }
    }
}
